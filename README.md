# Flyer pour la monnaie libre Ğ1

Flyer vulgarisant rapidement le problème de la monnaie dette et la solution qu'apporte la monnaie libre, ainsi que les actions concrètes à mener.

Adapté à tous, particulièrement à un public de gauche. Idéal en manifestation.

Disponible en français et espéranto.

## Fabrication

Télécharger la version voulue :

 * [PDF Asturien (ast)](https://zettascript.org/tux/g1/flyer/flyer-g1-ast.pdf)
 * [PDF Espagnol (es)](https://zettascript.org/tux/g1/flyer/flyer-g1-es.pdf)
 * [PDF Espéranto (eo)](https://zettascript.org/tux/g1/flyer/flyer-g1-eo.pdf)
 * [PDF Français (fr)](https://zettascript.org/tux/g1/flyer/flyer-g1-fr.pdf)
 * [PDF Italian (it)](https://zettascript.org/tux/g1/flyer/flyer-g1-it.pdf)

Imprimer en couleur en recto-verso sur A4 blanc, orientation paysage. Le bas de chaque page doit être du même côté de la feuille.

Plier en 3 selon les pointillés :

 * Poser la feuille avec la page de titre (gros logo Ğ1 rond jaune) côté table
 * Plier le tiers droit sur le tiers central, en laissant de la marge pour plier le tiers gauche
 * Plier le tiers gauche sur le reste. Le titre doit être visible quand le flyer est plié.

## Contribuer

Vous pouvez l'adapter pour votre région, ou traduire le flyer dans d'autres langues.

Une version récente de Scribus est nécessaire (>= 1.5). Les utilisateurs de distributions Linux comme Debian devront télécharger Scribus depuis [les backports](https://wiki.debian.org/Backports) ou [le site officiel](https://scribus.net).

## Conception

Réalisé avec les logiciels libres suivants :
 * [**Scribus**](https://www.scribus.net/) : conception graphique du flyer
 * [**Gimp**](https://www.gimp.org/) : export des SVG en PNG
 * [**La TRM en Transition**](http://cuckooland.free.fr/LaTrmEnTransition.html?N4IgtiBcoGZSAhAhgZwJYGMCqAREAaEAJ3iQDcBTIpAcwoOJSlDAFdmQAbAexqhiScUFAL6FWAEw48+kAUNGEwAO2m9+g4SLFd6kABwAGcRMNQAjIauEALhEjGQNgLIWAbOcIYMGhV-aQIACaAKIAggBKDAAOAcHhUV5QhgB0niBSgQBy3Mr0hBIAnq6QllaOEgAe9hWVJUYFlQAKZFAALBXRUADs+o1MkG2ESD6Qbrajjkhm0CBomekARhYFUA0g08m28ADCOyA6SOYc81AATITLpasGUzNl24F7B8NnJ5kAzJcrGWt359YnLt9ocPu92t8DOlMutNg5HiBnoc2uDIABWSH6aF-YYzRw2YEvDZo1HjEBXLE3WF4hFInSyUAEwIRCgwKgUZQYCgAFSQyySgWUuXyG1GoAAFgMANqijDcVjKGxvYbeeWKlEquUKmwkzVqmxuBgoGx8zgUMKUah0EAAXR0EjFIElUBlxtNFBwaDI8w5UjthHFjudkCl-pAKCD0vDJsWZucwpNREKAGVWNFopxCradBgmSBk2EGEgtk4SJAALTpAYgBhoKCVrzcewgFlsogcrm85baIA) : données du graphique sur la convergence et la fonte
 * [**Python**](https://python.org) : formatage des données du graphique sur la convergence et la fonte
 * [**GnuPlot**](http://www.gnuplot.info/) : graphique sur la convergence et la fonte
 * [**wotmap**](https://git.duniter.org/paidge/wotmap) : image de la toile de confiance
 * [**WorldWotMap**](https://zettascript.org/tux/g1/worldwotmap) : carte de la toile de confiance
 * [**instance Discourse Monnaie Libre**](https://forum.monnaie-libre.fr) : retours constructifs

Réalisé avec les ressources libres suivantes :
 * [**Linux Libertine**](http://libertine-fonts.org/) : une police sublime
 * [**Proza Libre**](https://fontlibrary.org/en/font/proza-libre) : une police élégante et moderne
 * [**Domestic Manners**](https://www.1001fonts.com/domestic-manners-font.html) : une police manuscrite utilisée pour le Geektionnerd
 * [**Carlito**](https://fontlibrary.org/en/font/carlito) : une police simple et assez moderne (utilisée quand Proza Libre n'est pas assez étendue)
 * [Logo Ğ1-flare](https://git.duniter.org/vincent/G1/blob/master/logos/G1-flare/)
 * [Logo Duniter](https://git.duniter.org/nodes/typescript/duniter/blob/dev/images/duniter-logo.svg) (pas encore utilisé, mais peut-être)
 * [Logo g1-june](https://axiom-team.fr/les-logos/)
 * [Logo CC0](https://commons.wikimedia.org/wiki/File:CC-Zero-badge.svg)
 * [Les autres flyers existants](https://git.duniter.org/vincent/G1/tree/master/flyers)
 * [Un tuto pour Scribus sur PeerTube](https://peertube.mastodon.host/videos/watch/0e697d5d-51f9-497b-9f81-0003d6d93d0a)
 * [Un article pour GnuPlot](http://www.gnuplotting.org/attractive-plots/)
 * [Des images du Geektionnerd](https://framagit.org/framasoft/fun/geektionnerd-generator)
 * [Drapeau Espéranto](https://commons.wikimedia.org/wiki/File:Flag_of_Esperanto.svg)
 * [**OpenStreetMap**](https://openstreetmap.org) : données carte du monde
 * [**Toner Lite**](https://maps.stamen.com) : tuiles de la carte du monde

Remerciements spéciaux à Vivakvo pour la correction de la traduction en espéranto, et à Kapis pour les traductions en espagnol, asturien et italien.

Réalisé par tuxmain (Pascal Engélibert) <tuxmain@zettascript.org>  
Licence CC0 (domaine public)
