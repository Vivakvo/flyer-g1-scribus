#/usr/bin/env python3

import igraph

graph = igraph.Graph(directed=True)

graph.add_vertices(10)
graph.add_edges([
	(0,1),
	(0,2),
	(0,3),
	
	(1,2),
	(1,0),
	(1,4),
	(1,5),
	
	(2,9),
	
	(3,6),
	(3,7),
	
	(4,5),
	
	(6,8),
	
	(8,7),
	(8,9),
	
	(9,8)
])

igraph.plot(graph, "wot-graph.svg",
	vertex_size = 16,
	vertex_color = "red",
	edge_width = 1,
	layout = graph.layout("kk"),
	bbox = (320, 240),
	margin = 30,
	autocurve = True,
	edge_arrow_size = 1,
	edge_arrow_length = 1
)
